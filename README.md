
# desafio-tecnico-backend
Projeto realizado para o desafio técnico da Cubos.

## Get started
Install

    $ yarn install


Run

    $ yarn server

## Rules
- Cadastrar regras de horários para atendimento
- Apagar regra de horário para atendimento
- Listar regras de horários para atendimento
- Listar horários disponíveis dentro de um intervalo

## API Service

### `/api/v1/scales`
* **Method:** GET
* **Type:** application/json
* **Return values:**
  * `result.status`: 200 - A comunicação deu certo
  * `result.data`: JSONArray preenchidos ou não com as regras registradas
  * qualquer outra - Problemas de execuçãoe comunicação
* **Description:** Retorna todas as regras cadastradas no sistema.

### `/api/v1/scales`
* **Method:** POST
* **Type:** application/json
* **Return values:**
  * `result.status`: 200 - A comunicação deu certo
  * `result.data._id`: Confirma que a nova regra foi armazenada
  * `result.err`: Problema identificado na validação de dados
  * qualquer outra - Houve problema ao tentar registrar a nova regra.
* **Description:** Cria uma nova regra no sistema
* **Obs:** Os dados devem ser passados através do corpo da requisição. Verifique a sessão  `Padrões para criação de regra`.

### `/api/v1/scales/:id`
* **Method:** DELETE
* **Type:** application/json
* **:id [param]** Id de referência da regra que deve ser removida
* **Return values:**
  * `result.status`: 200 - A comunicação deu certo
  * `result.err`: Problema identificado na validação de dados
  * Qualquer outra - Problemas ao tentar remover a regra
* **Description:** Remove uma regra a partir de uma uri com o id

### `/api/v1/scales/:data_inicio/to/:data_fim`

* **Method:** GET
* **Type:** application/json
* **:data_inicio** Data inicial que deve ser aplicada no filtro no formato: `DD-MM-YYYY`
* **:data_FIM** Data final que deve ser aplicada no filtro no formato: `DD-MM-YYYY`
* **Return values:**
  * `result.status`: 200 - A comunicação deu certo
  * `result.data`: JSONArray - Uma lista preenchida ou vazia contendo os dias e as datas disponíveis com base nas regras registradas no sistema
  * `result.startDate`: Data inicial baseada para o filtro
  * `result.endDate`: Data final baseada para o filtro
  * `result.err`: Problema identificado na validação de dados
* **Description:** Retorna os horários disponíveis considerando um intervalo de datas informadas na requisição.

## Padrões para criação de regra
As informações para a criação de uma nova regra devem ser enviadas no corpo da requisição em forma de objeto JSON. Veja abaixo:

```js
{
	"detail": "Dia específico",
	"specificDate": "22-11-2018",
	"interval" : [
        {"start": "14:30", "end": "15:00"},
        {"start": "15:00", "end": "15:30"}
    ]
} //- OBS.: Esta regra se aplica apenas ao dia 22 de novembro de 2018
```


| Key          | Tipo             | Informação                                                                                                                                                |
|--------------|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| detail       | `String`           | Informar detalhes se preciso da nova regra                                                                                                                |
| specificDate | `String`           | Uma data no formato `DD-MM-YYYY` registra a regra apenas para esta data informada. **Obs.:** O uso desta chave invalida a chave repeat                          |
| interval     | `Array`            | Uma lista contendo um ou mais objetos JSON com informações dos intervalos de horas seguindo este formato: `{"start": "HH:MM", "end": "HH:MM"}`              |
| repeat       | `Boolean` ou `Object` | Esta chave pode conter um valor Boolean `true` ou `false` informando que esta regra se repetirá todos os dias ou um JSONObjeto com regras para as repetições. |

#### Regras de repetição

As regras de repetição são aplicados na chave repeat do objeto enviado no corpo da requisição. Caso a chave repeat contenha apenas um valor Boolean, a regra informada se repetirá todos os dias. Veja abaixo:

```js
{
	"detail": "Diariamente",
	"repeat" : true, // Regra se aplica a todos os dias
	"interval" : [
        	{"start": "09:30", "end": "10:10"}
    	]
}
```

Além de boolean a regra de repetiação pode ser manipulada através de um objeto JSON contendo as chaves:

| Key          | Tipo    | Informação                                                                                                                                                                                  |
|--------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| daysOfWeek   | `Array`   | Lista contendo os dias da semana que a regra deve ser aplicada. Cada dia da semana é indicado por um número que vai de 1 a 7, sendo 1 = "Domingo", 4 = "Quarta"  e 7 referencia ao "Sábado" |
| weeksOfMonth | `Array`   | Lista contendo as semanas do mês que a regra deve ser aplicada. As semanas vão de 1 a 5 dividindo os dias do mês. Cada semana pode ir conter de 1 a 7 dias.                                 |
| monthly      | `Boolean` | Indica se as regras de repetição serão aplicadas a cada mês                                                                                                                                 |
| specificDay  | `Number`  | É usada quando a chave monthly contém o valor positivo (true). Isso indica que esta regra é aplicada todos os meses neste dia específico  |

##### Exemplos:
**1**: Este exemplo indica que esta regra será aplicada em todas as segundas e quartas de cada mês.
```js
{
	"detail": "Semanalmente",
	"repeat" : {
		"daysOfWeek" : [2, 4],
        "weeksOfMonth" : [1, 2, 3, 4, 5]
	},
	"interval" : [
        	{"start": "14:00", "end": "14:30"}
    	]
}
```

**2**: Este exemplo indica uma regra que se aplicará nas quintas e sextas da segunda e quarta semana de cada mês.

```js
{
	"detail": "Quinzenalmente",
	"repeat" : {
        	"daysOfWeek" : [5, 6],
        	"weeksOfMonth" : [2, 4]
    	},
    	"interval" : [
        	{"start": "10:30", "end": "11:00"},
        	{"start": "07:00", "end": "08:00"}
    	]
}
```

**3**: Aqui a regra se aplicará em toda primeira segunda-feira de cada mês.

```js
{
	"detail": "Mensalmente (Toda primeira segunda do mês)",
	"repeat" : {
        	"monthly" : true,
        	"daysOfWeek" : [2],
        	"weeksOfMonth" : [1]
    	},
    	"interval" : [
        	{"start": "08:30", "end": "08:50"}
    	]
}
```

**4**: Neste próximo exemplo a regra indicada aplica-se à todo dia 12 de cada mês.

```js
{
	"detail": "Todo dia 12 de cada mês",
	"repeat" : {
        	"monthly" : true,
        	"specificDay" : 12
    	},
    	"interval" : [
        	{"start": "08:30", "end": "08:50"}
    	]
}
```

## News
- Rules validation
- Intervals auto order
- Automatic sorting of intervals

## Informações
**Autor:** Gleisson Ricado
**Email:** contato.mattos@outlook.com
**Github** github.com/gleissonmattos
