import * as http from 'http';
import Server from './server';

// Server port setup
const port = portSetup(process.env.PORT || 3000);
Server.set('port', port);
console.log(`Server running on ${port}`);

// Create server
const server = http.createServer(Server);
server.listen(port);
server.on('error', listenError);
server.on('listening', listen);

/**
* Normalize port on setup server starte
* @param {umber | string): number | string | boolean} val - Port value
* @return - port value or 'false' boolean value on problem
*/
function portSetup(val: number | string): number | string | boolean {
  const port: number = typeof val === 'string' ? parseInt(val, 10) : val;
  if (isNaN(port)) {
    return val;
  } else if (port >= 0) {
    return port;
  } else {
    return false;
  }
}

/**
* Error listener on try running Server
* @param {NodeJS.ErrnoException} err - Listen error object
*/
function listenError(err: NodeJS.ErrnoException): void {
  if (err.syscall !== 'listen') {
    throw err;
  }
  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
  switch (err.code) {
    case 'EACCES':
      console.error(`${bind} requires privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw err;
  }
}

/**
* onListener on try running Server
* @param {NodeJS.ErrnoException} err - Listen error object
*/
function listen(): void {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
}
