import DateHelper from '../util/DateHelper';

interface Interval {
    start: string;
    end: string;
}

/**
* DateHelper class
*/
class IntervalParser {

    static get START_DAY_HOUR() { return "00:00" };

    /**
    * Check valides intervals
    * @param {Array} interval - All intervals
    */
    static intervalsAreValids(interval) {
        for (let i = 0; i < interval.length; i++) {
            let inter = interval[i];
            if (DateHelper.hourToNumber(inter.start) >=
                DateHelper.hourToNumber(inter.end)) {
                return false;
            }
        }
        return true;
    }

    /**
    * Get intervals diference
    * Ex.:
    * If intervals param is: [{start: '08:00', end: '10:00'}]
    * RETURN is [{start: 00:00, end: 08:00}, {start: 10:00, end: 00:00}]
    *
    * @param {Array} Intervals the rules
    * @return {Array} Intervals diference
    */
    static getDiffIntervals(intervals) {
        if (intervals.length == 0) return [];

        intervals = intervals.sort((a, b) => {
            if (DateHelper.hourToNumber(a.start) < DateHelper.hourToNumber(b.start))
                return -1;
            if (DateHelper.hourToNumber(a.start) < DateHelper.hourToNumber(b.start))
                return 1;
            return 0;
        });

        var horas = intervals;
        var currentHour = this.START_DAY_HOUR;
        var disp = [];

        for (var i = 0; i < horas.length; i++) {
            var interval = {};
            if (currentHour != horas[i]['start']) {
                interval['start'] = currentHour;
                interval['end'] = horas[i]['start'];
                disp.push(interval);

                currentHour = horas[i]['end'];
            }
        }

        var lastInterval = {};
        lastInterval['start'] = horas[horas.length - 1]['end'];
        lastInterval['end'] = this.START_DAY_HOUR;
        disp.push(lastInterval);
        return disp;
    }

    /**
    * Interval format order
    * @param {Array<Object>} interval - string date
    * @param {Array} interval - Interval to order
    * @return - the interval ordened
    */
    static intervalOrder(interval: Array<Object>): Array<Object> {
        return interval.sort((a: Interval, b: Interval) => {
            return a.start < b.start ? -1 : a.start > b.start ? 1 : 0;
        });
    }
}

// export
export default IntervalParser;
