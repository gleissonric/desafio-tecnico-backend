import { Schema, model } from 'mongoose';

export const ScaleSchema: Schema = new Schema({
    info: {
        type: String,
        default: ''
    },
    specificDate: Date,
    interval: [{
        start : {
            type: String,
            validate: /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/,
            required: true
        },
        end : {
            type: String,
            validate: /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/,
            required: true
        }
    }],
    repeat: {
        monthly: {
            type: Boolean,
            default: false
        },
        specificDay: Number,
        daysOfWeek: Array,
        weeksOfMonth: Array,
    }
});
