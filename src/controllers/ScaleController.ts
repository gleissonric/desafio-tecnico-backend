import * as mongoose from 'mongoose';
import {
    ScaleSchema
} from '../models/Scale';

import {
    Request,
    Response,
} from 'express'

import DateHelper from '../util/DateHelper';
import IntervalParser from '../parser/IntervalParser';
const Scale = mongoose.model('Scale', ScaleSchema);

interface Interval {
    start: string;
    end: string;
}

/**
 * ScaleController class
 */
export class ScaleController {


    /**
    * Check conflicts in intervals
    * @param {Interval} ruleInterval - Rule object
    * @param {Array<Interval>} interval - Interval to check
    * @param {Number | null} param with position to ignore
    * @return - true or false if conflict exists
    */
    private checkIntervalConflictIgn(
        ruleInterval: Interval, interval: Array<Interval>, ignorePosition: Number | null): Boolean {
        let inter: Interval;
        let ignore: Boolean;
        for (let i = 0; i < interval.length; i++) {
            ignore = false;

            if(ignorePosition != null)
                if(ignorePosition == i)
                    ignore = true

            if(!ignore) {
                inter = interval[i]
                if (DateHelper.hourToNumber(inter.start) < DateHelper.hourToNumber(ruleInterval.start)) {
                    if (DateHelper.hourToNumber(inter.end) > DateHelper.hourToNumber(ruleInterval.start)) {
                        return false;
                    }
                } else if(DateHelper.hourToNumber(inter.start) == DateHelper.hourToNumber(ruleInterval.start)) {
                    return false
                } else if (DateHelper.hourToNumber(inter.start) < DateHelper.hourToNumber(ruleInterval.end)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
    * Check conflicts in intervals
    * @param {Object} ruleInterval - Rule object
    * @param {Array} interval - Interval to check
    * @return - true or false if conflict exists
    */
    private checkIntervalConflict(ruleInterval: Interval, interval: Array<Interval>): Boolean {
        return this.checkIntervalConflictIgn(ruleInterval, interval, null);
    }

    /**
     * Dates occurrence in weeks and days
     * @param {String} specificDate - string date
     * @param {Array} interval - Interval to check
     * @return - true or false if conflict exists
     */
    private checkDatesOccurrence(specificDate, weeks, days): Boolean {
        for (let dateStr of specificDate) {
            const date = new Date(dateStr);
            let week = DateHelper.getWeekOfMonth(date);
            let day = DateHelper.dayOfWeek(date);

            if (weeks.includes(week) && days.includes(day))
                return true; // Contains occurrence
        }
        return false;
    }

    /*
     * CREATE one new rule on database
     */
    public createScaleRule(req: Request, res: Response): void {
        // Get body params
        const {
            detail,
            specificDate,
            repeat,
            interval
        } = req.body;
        var prepareScale = {};

        if (typeof specificDate === 'string') {
            // specific date ->
            const part = specificDate.split('-');

            if (!DateHelper.isValidDate(specificDate)) {
                this.reportValidate(res, {
                    message: 'Data inválida. ' +
                        '[specificDate] deve seguir o formato: DD-MM-YYYY'
                });
                return;
            }

            let specDate = DateHelper.createDate(specificDate);

            // define date value on object
            prepareScale['specificDate'] = specDate;
            prepareScale['repeat'] = {
                monthly: false,
                daysOfWeek: [],
                weeksOfMonth: []
            };
        } else {
            if (typeof repeat === 'boolean') {
                // if repeat all days
                prepareScale['repeat'] = {
                    monthly: false,
                    // define all week days
                    daysOfWeek: DateHelper.WEEK_DAYS,
                    // define total weeks on mounth
                    weeksOfMonth: DateHelper.WEEK_THE_MOUNT
                };
            } else if (typeof repeat === 'object') {
                let repeatRule = {}
                // Check monthly repeat key
                repeatRule['monthly'] = typeof repeat.monthly === 'boolean' ?
                    repeat.monthly : false;

                if (repeat.specificDay && repeatRule['monthly']) {
                    // repeat one specific day on month
                    repeatRule['specificDay'] = repeat.specificDay;
                } else {
                    // repeat with week rules
                    repeatRule['daysOfWeek'] = repeat.daysOfWeek;
                    repeatRule['weeksOfMonth'] = repeat.weeksOfMonth;
                }
                prepareScale['repeat'] = repeatRule;
            } else {
                this.reportValidate(res, {
                    message: 'A chave "repeat" deve ser um Boolean ou um objeto com regras.' +
                        'Verifique a documentação'
                });
                return;
            }
        }
        prepareScale['info'] = detail;

        // order intervals
        prepareScale['interval'] = IntervalParser.intervalOrder(interval);

        // Validate interval interval
        for(let i = 0; i < prepareScale['interval'].length; i++) {
            let intr = prepareScale['interval'][i];
            if (!this.checkIntervalConflictIgn(
                    intr, prepareScale['interval'], i)) {

                this.reportValidate(res, {
                    message: "Regra invlálida. Há conflitos nos intervalos desta regra"
                });
                return;
            }
        }

        let scale = new Scale(prepareScale);
        let err = scale.validateSync();

        // Validate schema
        if (err) {
            this.reportValidate(res, {
                message: "Regra inválida. Verifique as informações"
            });
            return;
        }

        // Check if contains intervals
        if (interval instanceof Array) {
            if (interval.length == 0) {
                // Report interval required
                this.reportValidate(
                    res, 'A regra deve conter ao menos um intervalo'
                );
                return;
            } else {
                if (!IntervalParser.intervalsAreValids(interval)) {
                    this.reportValidate(
                        res, 'A hora final dos intervalos deve ser maior que a inicial'
                    );
                    return;
                }
            }
        }

        Scale.aggregate([{
                    "$match": {
                        "$or": [{
                                "specificDate": {
                                    "$ne": null
                                }
                            },
                            {
                                "repeat.daysOfWeek": {
                                    "$in": prepareScale['repeat']['daysOfWeek']
                                    &&  !prepareScale['specificDate'] ?
                                        prepareScale['repeat']['daysOfWeek'] :
                                        DateHelper.WEEK_DAYS
                                }
                            },
                            {
                                "repeat.monthly": true,
                                "specificDay": {
                                    "$ne": null,
                                    "$exists": true
                                }
                            }
                        ]
                    }
                },
                {
                    "$unwind": "$interval"
                },
                {
                    "$group": {
                        "_id": null,
                        "specificDate": {
                            "$push": "$specificDate"
                        },
                        "repeat": {
                            "$push": "$repeat"
                        },
                        "interval": {
                            "$push": "$interval"
                        }
                    }
                },
                {
                    "$project": {
                        "interval._id": 0
                    }
                }
            ]).then((allRules) => {
                if (allRules.length > 0) {
                    const rules = allRules[0];
                    let interator = rules.interval.length;
                    for (let i = 0; i < interator; i++) {
                        let valid = true;
                        // get news weeks and days
                        let days = prepareScale['repeat']['daysOfWeek']
                        let weeks = prepareScale['repeat']['weeksOfMonth'];

                        // get rules week and days
                        let ruleWeeks = rules.repeat[i]['weeksOfMonth'];
                        let ruleDaysWeek = rules.repeat[i]['daysOfWeek'];
                        // rules contains weeks and days of week
                        if (prepareScale['repeat']['monthly']) {
                            if (!this.checkIntervalConflict(
                                    rules.interval[i], prepareScale['interval'])) {
                                valid = false;
                            }
                        } else if (ruleWeeks.length > 0 && ruleDaysWeek.length > 0) {
                            // verificar a partir dos dias e semenas
                            let ruleWeeks = rules.repeat[i]['weeksOfMonth'];

                            if (prepareScale['specificDate']) {
                                // Check new rule specific date on week rules
                                if (this.checkDatesOccurrence(
                                    [prepareScale['specificDate']], ruleWeeks, ruleDaysWeek)) {
                                    if (!this.checkIntervalConflict(
                                            rules.interval[i], prepareScale['interval'])) {
                                        valid = false;
                                    }
                                }
                            } else if (ruleWeeks.some(r => weeks.indexOf(r) >= 0)) {
                                if (!this.checkIntervalConflict(
                                        rules.interval[i], prepareScale['interval'])) {
                                    valid = false;
                                }
                            }
                        } else {
                            if (rules['specificDate'].length > 0) {
                                // Check specific date if contains ocurrences
                                if (prepareScale['specificDate']) {
                                    if (new Date(rules['specificDate'][i]).getTime() ==
                                        prepareScale['specificDate'].getTime()) {
                                        if (!this.checkIntervalConflict(
                                                rules.interval[i], prepareScale['interval'])) {
                                            valid = false;
                                        }
                                    }
                                } else {
                                    if (this.checkDatesOccurrence(rules['specificDate'], weeks, days)) {
                                        if (!this.checkIntervalConflict(
                                                rules.interval[i], prepareScale['interval'])) {
                                            valid = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (!valid) {
                            // Report conflict message
                            this.reportValidate(res, {
                                message: 'Houve conflito entre os intervalos já registrados'
                            })
                            return;
                        }
                    }
                }
                // Save on database
                this.save(scale, res);
            })
            .catch((err) => {
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            });
    }

    private save(scale, res: Response): void {
        scale.save()
        .then((data) => {
            const status = res.statusCode;
            res.json({
                status,
                data
            });
        })
        .catch((err) => {
            const status = res.statusCode;
            res.json({
                status,
                err
            });
        });
    }

    /*
     * GET all rules on database
     */
    public getAllRules(req: Request, res: Response): void {
        Scale.find({}, {
                "__v": 0
            })
            .then((data) => {
                const status = res.statusCode;
                res.json({
                    status,
                    data
                });
            })
            .catch((err) => {
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            });
    }

    /*
     * GET disponibles hours by date range
     */
    public getHoursByRange(req: Request, res: Response): void {
        // get from params data
        const {
            startDate,
            endDate
        } = req.params;

        let end = (endDate) ? endDate : startDate;

        // Check if date is valid
        if (!(DateHelper.isValidDate(startDate) && DateHelper.isValidDate(end))) {
            this.reportValidate(res, {
                message: 'Data inválida. ' +
                    'As datas do filtro devem seguir o formato: DD-MM-YYYY'
            });
            return;
        }

        // Helpers
        let start = DateHelper.createDate(startDate);
        end = DateHelper.createDate(end);
        let daysOfWeek = DateHelper.daysOfWeek(start, end);

        // Prevent lower end date than initial
        if (daysOfWeek.length == 0) {
            this.reportValidate(res, {
                message: 'A data final deve ser igual ou superior á data inicial'
            });
            return;
        }
        // Find rules on database
        Scale.find({
                "$or": [{
                        "specificDate": {
                            "$gte": start,
                            "$lte": end
                        }
                    },
                    {
                        "repeat.daysOfWeek": {
                            "$in": daysOfWeek
                        }
                    },
                    {
                        "repeat.monthly": true,
                        "specificDay": {
                            "$ne": {
                                "$type": 10
                            }
                        }
                    }
                ]
            }, {
                "__v": 0,
                "_id": 0,
                "info": 0,
                "interval._id": 0
            }).then((data) => {

                // Get all dates in interval filter
                const allDates = DateHelper.getDatesInRanges(start, end);

                let out = [];
                allDates.forEach((date, index) => {
                    let dateItem = {};
                    let day = date.getDate();
                    let month = (date.getMonth() + 1);
                    let year = date.getFullYear();

                    // Get day key
                    const key = ('0' + day).slice(-2) + '-' + month + '-' + year;

                    dateItem['day'] = key;
                    dateItem['intervals'] = [];

                    // week helper
                    let weekOfMonth = DateHelper.getWeekOfMonth(date);
                    let dayOfWeek = DateHelper.dayOfWeek(date);
                    dateItem['weekOfMonth'] = weekOfMonth;
                    dateItem['dayOfWeek'] = dayOfWeek;

                    // Check filter
                    for (let i = 0; i < data.length; i++) {

                        if (data[i]['specificDate']) {
                            let specificDate = new Date(data[i]['specificDate']);
                            if (specificDate.getDate() === day &&
                                (specificDate.getMonth() + 1) === month &&
                                specificDate.getFullYear() == year) {
                                dateItem['intervals'] = dateItem['intervals'].concat(data[i]['interval']);
                            }
                        } else {
                            if (data[i]['repeat']['monthly'] && data[i]['repeat']['specificDay'] === day) {
                                dateItem['intervals'] = dateItem['intervals'].concat(data[i]['interval']);
                            } else if (data[i]['repeat']['daysOfWeek'].includes(dateItem['dayOfWeek']) &&
                                data[i]['repeat']['weeksOfMonth'].includes(dateItem['weekOfMonth'])) {
                                dateItem['intervals'] = dateItem['intervals'].concat(data[i]['interval']);
                            }
                        }
                    }
                    //dateItem['intervals'] = getDiffIntervals(dateItem['intervals']);
                    delete dateItem['weekOfMonth'];
                    delete dateItem['dayOfWeek'];

                    dateItem['intervals'] = IntervalParser.intervalOrder(dateItem['intervals']);
                    out.push(dateItem);
                    if ((index + 1) >= allDates.length) {
                        const status = res.statusCode;
                        res.json({
                            status,
                            startDate,
                            endDate,
                            /*daysOfWeek,*/
                            data: out,
                        });
                    }
                });
            })
            .catch((err) => {
                const status = res.statusCode;
                res.json({
                    status,
                    err
                });
            });
    }

    /*
     * DELETE on rule by ID
     */
    public deleteRuleById(req: Request, res: Response): void {
        const {
            id
        } = req.params;

        Scale.remove({
                _id: id
            })
            .then((data) => {
                const status = res.statusCode;
                res.json({
                    status,
                    data: {
                        deleted: data.n
                    }
                });
            })
            .catch((err) => {
                const status = res.statusCode;
                res.json({
                    status,
                    data: {
                        deleted: 0
                    },
                    err: {
                        message: "Verifique a identificação da regra"
                    }
                });
            });
    }

    /*
     * Report ERRROR to Response
     * @param {Response} res - Express Object Response
     * @param {String | Object} - Err message
     */
    private reportValidate(res: Response, message: String | Object): void {
        const status = res.statusCode;
        res.json({
            status,
            err: message
        })
    }

}
