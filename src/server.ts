import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';
import * as mongoose from 'mongoose';
import * as logger from 'morgan';

// import routers
import ScaleRouter from './router/ScaleRouter';

/**
* Server class
*/
class Server {

    // express application to Server
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    /**
    * Database server connection
    */
    public databaseConnect() {
        const URI: string = 'mongodb://master:Glrisamaneed2RR@ds043457.mlab.com:43457/cubos';
        mongoose.connect(URI || process.env.MONGODB_URI);
    }

    /**
    * Setup server specifications and access
    */
    public config() {

        // set up mongoose
        this.databaseConnect();

        // middleware
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(logger('dev'));
        this.app.use(compression());
        this.app.use(cookieParser());
        this.app.use(helmet());
        this.app.use(cors());

        // cors
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
          res.header('Access-Control-Allow-Origin', '*');
          res.header(
            'Access-Control-Allow-Methods',
            'GET, POST, PUT, DELETE, OPTIONS',
          );
          res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
          );
          res.header('Access-Control-Allow-Credentials', 'true');
          next();
        });
    }

    public routes() {
        let router: express.Router;
        router = express.Router();

        this.app.use('/', router);
        this.app.use('/api/v1/scales', ScaleRouter.router);
    }
}

// export
export default new Server().app;
