/**
* Express router providing user related routes
 * @module router/ScaleRoutes
 * @requires express
 */

 /**
 * express module
 * @const
 */
import {
    Router,
    Request,
    Response,
    NextFunction
} from 'express'

import { ScaleController } from '../controllers/ScaleController';
const controller = new ScaleController();

/**
* ScaleRouter class
*/
class ScaleRouter {

    public router: Router;
    public controller: ScaleController;

    constructor() {
        this.router = Router();
        this.routes();
    }

    /*
    * @method DELETE
    * Delete one rule by ID
    */
    public DeleteRule(req: Request, res: Response): void {
        controller.deleteRuleById(req, res);
    }

    /*
    * @method POST
    * Create one new rule
    */
    public CreateRule(req: Request, res: Response): void {
        controller.createScaleRule(req, res);
    }

    /*
    * @method GET
    * Get all rules
    */
    public GetRules(req: Request, res: Response): void {
        controller.getAllRules(req, res);
    }

    /*
    * @method GET
    * Get disponible hours by date range
    */
    public GetHoursByRange(req: Request, res: Response): void {
        controller.getHoursByRange(req, res);
    }

    /*
    * Route setup
    */
    public routes() {
        this.router.get('/', this.GetRules);
        this.router.post('/', this.CreateRule);
        this.router.delete('/:id', this.DeleteRule);
        this.router.get('/:startDate/to/:endDate', this.GetHoursByRange)
        this.router.get('/:startDate', this.GetHoursByRange) // One date
    }
}

// export
const scaleRoutes = new ScaleRouter();
scaleRoutes.routes();

export default scaleRoutes;
