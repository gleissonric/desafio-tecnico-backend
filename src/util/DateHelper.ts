import * as logger from 'morgan';

/**
* DateHelper class
*/
class DateHelper {

    static get WEEK_DAYS() { return [1, 2, 3, 4, 5, 6, 7] };

    static get WEEK_THE_MOUNT() {
        return [1, 2, 3, 4, 5];
    }

    // OBS.: Not check mounth to leap year occurrence
    static get WEEK_THE_MOUNT_LEAP_YEAR() {
        return this.WEEK_THE_MOUNT.push(5);
    }

    static createDate(date: String): Date {
        const dateValues = date.split('-');

        return new Date(
            parseInt(dateValues[2]),
            parseInt(dateValues[1]) - 1, // Adjust month JS index to correct
            parseInt(dateValues[0])
        );
    }

    static getDaysDiff(start: Date, end: Date) {
        return Math.round((end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24));
    }

    static daysOfWeek(start, end) {
        const daysOnWeek = 7;
        let daysDiff = this.getDaysDiff(start, end);
        let weekDay = start.getDay();
        let daysOfWeek = [];
        for (let i = 0; i < (daysDiff + 1); i++) {
            let day = weekDay + i;
            if (day > daysOnWeek) {
                weekDay = 1;
                day = weekDay;
            } else if (day <= 0) {
                day = daysOnWeek;
            }

            if (daysOfWeek.indexOf(day) === -1) {
                daysOfWeek.push(day);
            }
        }
        return daysOfWeek;
    }

    /**
    * Convert hour to float number
    * OBS.:
    * The time must be a formatted string with the hour and minute separated by ':'
    *
    * Ex.:
    * - String '12:25' => saída: 12.25 in number
    * - String '01:40' => saída: 1.40
    *
    * @param {date): Date - One target date
    * @return - The count week from target date
    */
    static hourToNumber(stringHour: String): Number {
        return parseFloat(stringHour.replace(':', '.'))
    }

    /**
    * Get week value of month
    * @param {date): Date - One target date
    * @return - The count week from target date
    */
    static getWeekOfMonth(date: Date): Number {
        return Math.ceil(date.getDate() / 7)
    }

    static getRealWeekOfMonth(date: Date): Number {
        var month = date.getMonth(),
        year = date.getFullYear(),
        firstWeekday = new Date(year, month, 1).getDay(),
        lastDateOfMonth = new Date(year, month + 1, 0).getDate(),
        offsetDate = 7 - firstWeekday,
        daysAfterFirstWeek = lastDateOfMonth - offsetDate,
        weeksInMonth = Math.ceil(daysAfterFirstWeek / 7) + 1;
        var week;
        var noOfDaysAfterRemovingFirstWeek = date.getDate() - offsetDate;
        if (noOfDaysAfterRemovingFirstWeek <= 0) {
            week = 1;
        } else if (noOfDaysAfterRemovingFirstWeek <= 7) {
            week = 2;
        } else if (noOfDaysAfterRemovingFirstWeek <= 14) {
            week = 3;
        } else if (noOfDaysAfterRemovingFirstWeek <= 21) {
            week = 4;
        } else if (weeksInMonth >= 5 && noOfDaysAfterRemovingFirstWeek <= 28) {
            week = 5;
        } else if (weeksInMonth === 6) {
            week = 6;
        }
        return week;
    }

    /*
    * Check if date is valid
    * @param {String} - date in format DD-MM-YYYY
    * @return - true or false
    */
    static isValidDate(s) {
      var bits = s.split('-');
      var d = new Date(bits[2], bits[1] - 1, bits[0]);
      return d && (d.getMonth() + 1) == bits[1];
    }

    /*
    * Check if hour is valid
    * @param {String} - hour in format HH:MM
    * @return - true or false
    */
    private hourIsValid(hour) {
        var isValid = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/.test(hour);
        return isValid;
    }

    /**
    * Get day of week
    * @param {date): Date - One target date
    * @return - The day of week from target date
    */
    static dayOfWeek(date: Date): Number {
        // Js adjustment day from date with -1
        return date.getDay() + 1;
    }

    /**
    * Get the dates ranges between two dates
    * @param {startDate): Date - Start date
    * @param {endDate): Date - End date
    * @return - One Array with dates ranges
    */
    static getDatesInRanges(startDate: Date, endDate: Date): Array<Date> {
        var dates = [],
            currentDate = startDate,
            addDays = function(days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    }

    static endFirstWeek(firstDate, firstDay) {
        if (! firstDay) {
            return 7 - firstDate.getDay();
        }
        if (firstDate.getDay() < firstDay) {
            return firstDay - firstDate.getDay();
        } else {
            return 7 - firstDate.getDay() + firstDay;
        }
    }

    /**
    * Get real weeks from month
    * @param {month): Number - Mês to check
    * @param {year): Number - Year
    * @return - One Arrya with real weeks
    */
    static getWeeksStartAndEndInMonth(month, year) {
        let weeks = [],
            firstDate = new Date(year, month, 1),
            lastDate = new Date(year, month + 1, 0),
            numDays = lastDate.getDate();

        let start = 1;
        let end = this.endFirstWeek(firstDate, 2);
        while (start <= numDays) {
            weeks.push({start: start, end: end});
            start = end + 1;
            end = end + 7;
            end = start === 1 && end === 8 ? 1 : end;
            if (end > numDays) {
                end = numDays;
            }
        }
        return weeks;
    }
}

// export
export default DateHelper;
