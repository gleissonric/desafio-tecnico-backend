# desafio-tecnico-backend
Nesta versão do Postman Collection para a API, as regras são exemplificadas
conforme os exemplos na documentação do desafio. Algumas regras do desafio
podem apresentar conflitos, ja que são validadas antes de ser armazenadas.

#### Exemplos:
- A regra **Diariamente** com a regra **Dia específico**:
    Os intervalos diários entram em conflito com um dos intervalos
    para a data: 25-06-2018:

    **Dia específico**
    intervalos: [`{"start": "09:30", "end": "10:20"}`, {"start": "10:30", "end": "11:00"}]
    **Diariamente**
    intervalos [`{"start": "09:30", "end": "10:10"}`]

- A regra **Dia específico** não entra em conflito com os intervalos da regra **Quinzenalmente** pois a data informada na primeira é o segundo dia da semana (Segunda-feira) e a segunda se aplica nas quintas e sextas da segunda e quarta semana dos meses.

    **Dia específico**
    intervalos: [{"start": "09:30", "end": "10:20"}, {"start": "10:30", "end": "11:00"}]
    25-06-2018 = segunda-feira

    **Quinzenalmente**
    intervalos: [{"start": "10:30", "end": "11:00"},{"start": "07:00", "end": "08:00"}]
    dias da semana: [5, 6] = (Quinta e sexta)

- Atlém da validação entre regras diferentes, os intervalos internos também são validados.
    Veja quando pode existir ocorrências

    **Regra**
    intervalos: [`{"start": "10:30", "end": "11:00"}`,`{"start": "09:00", "end": "11:30"}`, {"start": "14:25", "end": "14:50"}]
